# Application: HLSSimple #

This application fetches next timetable for Karhupuisto tram station shows the direction of the tram.

### How to run in local ###
Run the folowwing command in the root folder:

`mvn spring-boot:run`

### Future plans ###

Future plan for this project is to build a UI for the trams and a Magic Mirror (https://magicmirror.builders/)
where it is easy to see schedule for the trams