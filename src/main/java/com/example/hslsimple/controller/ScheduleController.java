package com.example.hslsimple.controller;

import com.example.hslsimple.dao.StopSchedule;
import com.example.hslsimple.facade.ScheduleFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

@Controller
public class ScheduleController {

    @Autowired
    private ScheduleFacade facade;

    @GetMapping("/getSchedule/{direction:1|0}")
    @ResponseBody
    public List<StopSchedule> sayHello(@PathVariable int direction) throws IOException {
        return facade.getSchedule(direction);
    }

}
