package com.example.hslsimple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HslsimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(HslsimpleApplication.class, args);
	}

}
