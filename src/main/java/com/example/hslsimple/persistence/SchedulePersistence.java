package com.example.hslsimple.persistence;

import java.io.IOException;
import java.util.Map;

import com.example.hslsimple.dao.Stop;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import org.springframework.stereotype.Repository;

import org.springframework.web.reactive.function.client.WebClient;

@Repository
public class SchedulePersistence {

    public static final String URL = "https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql";
    public static final String TO_HELSINKI = "HSL:1112434";
    public static final String FROM_HELSINKI = "HSL:1112433";

    public Stop getScheduleFromStop(int directions) throws IOException {
        if(directions == 1){
            return post(URL,TO_HELSINKI);
        }else{
            return post(URL, FROM_HELSINKI);
        }
    }

    public Stop post(String url, String stopNumber){
        ObjectMapper objectMapper = new ObjectMapper();
        WebClient webClient = WebClient.builder()
                .baseUrl(url)
                .build();

        GraphQLWebClient graphqlClient = GraphQLWebClient.newInstance(webClient, objectMapper);

        return graphqlClient.post("query.graphql",
                        Map.of("StopNumber", stopNumber), Stop.class)
                .block();
    }

  
}
