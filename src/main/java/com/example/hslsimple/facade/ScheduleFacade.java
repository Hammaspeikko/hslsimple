package com.example.hslsimple.facade;

import com.example.hslsimple.dao.Stop;
import com.example.hslsimple.dao.StopSchedule;
import com.example.hslsimple.dao.StoptimesWithoutPatternsItem;
import com.example.hslsimple.persistence.SchedulePersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScheduleFacade {

    @Autowired
    private SchedulePersistence persistence;
    public List<StopSchedule> getSchedule(int direction) throws IOException {
        return changeScheduleObjectForResponse(persistence.getScheduleFromStop(direction));
    }

    private List<StopSchedule> changeScheduleObjectForResponse(Stop stop){

        List<StopSchedule> stopScheduleList = new ArrayList<>();
        List<StoptimesWithoutPatternsItem> items = stop.getStoptimesWithoutPatterns();

        createScheduleList(stop, stopScheduleList, items);

        return stopScheduleList;
    }

    private void createScheduleList(Stop stop, List<StopSchedule> stopScheduleList, List<StoptimesWithoutPatternsItem> items) {
        for(StoptimesWithoutPatternsItem item : items){
            StopSchedule stopSchedule = new StopSchedule();

            String arrivalDate = getDateTime(item, item.getScheduledArrival());
            String realArrivalDate = getDateTime(item, item.getRealtimeArrival());

            stopSchedule.setArrivalTime(realArrivalDate);
            stopSchedule.setScheduledTime(arrivalDate);
            stopSchedule.setDirection(item.getTrip().getDirectionId());
            stopSchedule.setTram(item.getTrip().getRoute().getShortName());
            stopSchedule.setStopName(stop.getName());
            stopScheduleList.add(stopSchedule);
        }
    }

    private String getDateTime(StoptimesWithoutPatternsItem item, long arrivalTime) {
        long epochTimeArrival = item.getServiceDay() + arrivalTime;

        LocalDateTime localDate = Instant.ofEpochMilli(epochTimeArrival*1000)
                .atZone(ZoneId.systemDefault()).toLocalDateTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return localDate.format(formatter);
    }
}
