package com.example.hslsimple.dao;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class StopSchedule {

    private String tram;
    private String arrivalTime;
    private String scheduledTime;
    private String direction;
    private String stopName;
}
