package com.example.hslsimple.dao;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class StoptimesWithoutPatternsItem{

	private int scheduledArrival;

	private int realtimeArrival;

	private Trip trip;

	private int realtimeDeparture;

	private int serviceDay;

	private int arrivalDelay;

	private int scheduledDeparture;

	private int departureDelay;

}