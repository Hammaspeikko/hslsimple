package com.example.hslsimple.dao;

import java.util.List;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Stop{

	private List<StoptimesWithoutPatternsItem> stoptimesWithoutPatterns;

	private String name;


}