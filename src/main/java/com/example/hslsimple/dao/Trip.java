package com.example.hslsimple.dao;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Trip{

	private Route route;

	private String directionId;

}