package com.example.hslsimple.dao;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Route{

	private String shortName;

}