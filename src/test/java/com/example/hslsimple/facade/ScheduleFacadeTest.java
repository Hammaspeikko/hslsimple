package com.example.hslsimple.facade;

import com.example.hslsimple.dao.*;
import com.example.hslsimple.persistence.SchedulePersistence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ScheduleFacadeTest {

    @InjectMocks
    private ScheduleFacade facade;

    @Mock
    Stop stop;

    @Mock
    List<StoptimesWithoutPatternsItem> stoptimesWithoutPatternsItemList;

    @Mock
    Trip trip;

    @Mock
    Route route;

    @Mock
    StoptimesWithoutPatternsItem stoptimesWithoutPatternsItem;

    @Mock
    SchedulePersistence persistence;


    @BeforeEach
    void init() throws IOException {

        stop = new Stop();
        stop.setName("name");
        stoptimesWithoutPatternsItemList=new ArrayList<>();
        stoptimesWithoutPatternsItem.setServiceDay(1664053200);
        stoptimesWithoutPatternsItem.setScheduledArrival(64020);
        stoptimesWithoutPatternsItemList.add(stoptimesWithoutPatternsItem);
        stop.setStoptimesWithoutPatterns(stoptimesWithoutPatternsItemList);

        when(stoptimesWithoutPatternsItem.getTrip()).thenReturn(trip);
        when(trip.getRoute()).thenReturn(route);
        when(route.getShortName()).thenReturn("3");
        when(trip.getDirectionId()).thenReturn("1");
        when(persistence.getScheduleFromStop(1)).thenReturn(stop);
    }

    @Test
    void returnDataInCorrectFormat() throws IOException {

        List<StopSchedule> stopScheduleList = facade.getSchedule(1);

        assertAll("stopScheduleListValues",
                () -> assertEquals(stopScheduleList.get(0).getTram(), "3"),
                () -> assertEquals(stopScheduleList.get(0).getDirection(), "1"),
                () -> assertEquals(stopScheduleList.get(0).getArrivalTime(), "1970-01-01 02:00:00"),
                () -> assertEquals(stopScheduleList.get(0).getScheduledTime(), "1970-01-01 02:00:00"),
                () -> assertEquals(stopScheduleList.get(0).getStopName(), "name")
        );
    }

}
