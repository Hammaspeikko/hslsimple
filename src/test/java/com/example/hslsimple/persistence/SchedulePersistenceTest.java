package com.example.hslsimple.persistence;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class SchedulePersistenceTest {

    private final String json = "{\"data\":{\"stop\":{\"name\":\"Karhupuisto\",\"stoptimesWithoutPatterns\":[{\"scheduledArrival\":58020,\"realtimeArrival\":58076,\"arrivalDelay\":56,\"scheduledDeparture\":58020,\"realtimeDeparture\":58108,\"departureDelay\":88,\"serviceDay\":1664139600,\"trip\":{\"route\":{\"shortName\":\"9\"},\"directionId\":\"1\"}},{\"scheduledArrival\":58200,\"realtimeArrival\":58194,\"arrivalDelay\":-6,\"scheduledDeparture\":58200,\"realtimeDeparture\":58226,\"departureDelay\":26,\"serviceDay\":1664139600,\"trip\":{\"route\":{\"shortName\":\"3\"},\"directionId\":\"1\"}},{\"scheduledArrival\":58620,\"realtimeArrival\":58621,\"arrivalDelay\":1,\"scheduledDeparture\":58620,\"realtimeDeparture\":58653,\"departureDelay\":33,\"serviceDay\":1664139600,\"trip\":{\"route\":{\"shortName\":\"9\"},\"directionId\":\"1\"}},{\"scheduledArrival\":58800,\"realtimeArrival\":58744,\"arrivalDelay\":-56,\"scheduledDeparture\":58800,\"realtimeDeparture\":58778,\"departureDelay\":-22,\"serviceDay\":1664139600,\"trip\":{\"route\":{\"shortName\":\"3\"},\"directionId\":\"1\"}},{\"scheduledArrival\":59220,\"realtimeArrival\":59220,\"arrivalDelay\":0,\"scheduledDeparture\":59220,\"realtimeDeparture\":59220,\"departureDelay\":0,\"serviceDay\":1664139600,\"trip\":{\"route\":{\"shortName\":\"9\"},\"directionId\":\"1\"}}]}}}";

    @InjectMocks
    private SchedulePersistence persistence;

    @Test
    void returnDataInCorrectFormat() throws IOException, InterruptedException {
        MockWebServer server = new MockWebServer();

        server.enqueue(new MockResponse().setBody(json));
        server.start();

        HttpUrl baseUrl = server.url("/this/is/a/test");

        persistence.post(baseUrl.toString(), "123");

        RecordedRequest request1 = server.takeRequest();
        assertEquals("/this/is/a/test", request1.getPath());
        server.shutdown();
    }

    @Test
    void callReturns500() throws IOException, InterruptedException {
        MockWebServer server = new MockWebServer();

        server.enqueue(new MockResponse().setResponseCode(500));

        server.start();

        HttpUrl baseUrl = server.url("/this/is/a/test");

        WebClientResponseException.InternalServerError thrown = Assertions.assertThrows(WebClientResponseException.InternalServerError.class, () ->{
            persistence.post(baseUrl.toString(), "123");
        });

        RecordedRequest request1 = server.takeRequest();
        assertEquals("/this/is/a/test", request1.getPath());
        server.shutdown();
    }
}
