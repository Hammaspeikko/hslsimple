package com.example.hslsimple.controller;

import com.example.hslsimple.dao.StopSchedule;
import com.example.hslsimple.facade.ScheduleFacade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ScheduleController.class)
class ScheduleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScheduleFacade facade;

    @Test
    void shouldBeSuccessful_WithParam_1() throws Exception {

        List<StopSchedule> stopScheduleList = CreateTestData();
        when(facade.getSchedule(1)).thenReturn(stopScheduleList);

        this.mockMvc.perform(get("/getSchedule/1")).andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("This is a test")));}


    @Test
    void shouldBeSuccessful_WithParam_0() throws Exception {
        List<StopSchedule> stopScheduleList = CreateTestData();
        when(facade.getSchedule(0)).thenReturn(stopScheduleList);
        this.mockMvc.perform(get("/getSchedule/0")).andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("This is a test")));}

    @Test
    void shouldThrowNotFound() throws Exception {
        this.mockMvc.perform(get("/getSchedule/3")).andDo(print()).andExpect(status().isNotFound());}


    private static List<StopSchedule> CreateTestData() {
        List<StopSchedule> stopScheduleList = new ArrayList<>();
        StopSchedule stopSchedule = new StopSchedule();
        stopSchedule.setStopName("This is a test");
        stopScheduleList.add(stopSchedule);
        return stopScheduleList;
    }

}
